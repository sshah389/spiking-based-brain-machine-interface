import numpy as np
import csv
import time
from matplotlib import pyplot as plt

from closed_loop_simulator import CLS
from online_prosthetic_simulator import OPS
from snn_continuous_learning import snn
import pickle

def get_trial(cls,max_duration):
    cls.start_trial()
    vels = np.zeros((max_duration,2))
    accels = np.zeros((max_duration,2))
    t = 0
    t_in_range = 0
    while t < max_duration and t_in_range < time_to_target:
        vels[t,:],accels[t,:] = cls.get_velocity()
        mag = np.linalg.norm(vels[t,:])
        cls.update_pos(vels[t,:] + np.random.normal(loc=0,scale=0.1*mag,size=(2,)))
        t, t_in_range = cls.get_times()

    vels = vels[:t,:]
    accels = accels[:t,:]
    
    return vels,accels

def get_spikes(ops,accels):
    spikes = np.zeros((len(accels),ops.num_neurons))
    for t in range(len(accels)):
        spikes[t,:] = ops.get_spikes(accels[t])

    return spikes

def get_correlation(total_output,label):

    r = np.corrcoef([total_output[:,0],label[:,0]])
    r_x = r[0,1]

    r = np.corrcoef([total_output[:,1],label[:,1]])
    r_y = r[0,1]

    return r_x,r_y

device = 'cpu'

time_step = 0.01
max_duration = int(3/time_step)
side_radius = 10
max_vel = 20*time_step #cm/s
accel_const = 0.3
min_distance = 8
target_size = 2.5
time_to_target = 0.5/time_step

vel_scale = 3.77

train_trials = 400
test_trials = 100

cls = CLS(side_radius=side_radius,min_distance=min_distance,max_velocity=max_vel,
          accel_const=accel_const,target_size=target_size)

neurons = 46
learning_rate = 5e-3
ops = OPS(neurons,time_step,upper_lmin=5,lower_lmax=40,upper_lmax=100,
          max_accel=accel_const*max_vel,zero_prob=0.5)

layers = [neurons, 65, 40]
output_neurons = 2


vels_train = np.zeros((1,2))
spikes_train = np.zeros((1,neurons))
vels_test = np.zeros((1,2))
spikes_test = np.zeros((1,neurons))

for trial in range(train_trials+1):
    vels,accels = get_trial(cls,max_duration)
    vels_train = np.concatenate((vels_train,vels),axis=0)
    spikes = get_spikes(ops,accels)
    spikes_train = np.concatenate((spikes_train,spikes),axis=0)

for trial in range(test_trials+1):
    vels,accels = get_trial(cls,max_duration)
    vels_test = np.concatenate((vels_test,vels),axis=0)
    spikes = get_spikes(ops,accels)
    spikes_test = np.concatenate((spikes_test,spikes),axis=0)


print("Start training")
model = snn(layers,output_neurons,300,learning_rate=learning_rate,
            tau_mem=20*time_step,tau_syn=0.3*time_step,time_step=time_step)

vels_train = vels_train / time_step
vels_test = vels_test / time_step
vels_train_scaled = vels_train / vel_scale

train_output = model.forward(spikes_train,vels_train_scaled,train=True)[-1]
print("Start testing")
test_output = model.forward(spikes_test,vels_test,train=False)[-1]

train_output = train_output*vel_scale
test_output = test_output*vel_scale


spike_rates = spike_rates / (len(total_output)*time_step)
np.set_printoptions(suppress=True)
print(spike_rates)
print(np.mean(spike_rates))
print()


corr_x,corr_y = get_correlation(test_output,vels_test)
print(f'X Correlation: {corr_x}, Y Correlation: {corr_y}')

weights = model.get_weights()
ro_weights = model.get_ro_weights()
np.save('closed_loop_w0_400.npy',weights[0])
np.save('closed_loop_w1_400.npy',weights[1])
np.save('closed_loop_ro0_400.npy',ro_weights[0])
np.save('closed_loop_ro1_400.npy',ro_weights[1])

ops.save_neurons('ops_neurons_400.csv')


t = np.arange(0,len(test_output))*time_step
plt.subplot(2,1,1)
plt.plot(t,vels_test[:,0],'b')
plt.plot(t,test_output[:,0],'r')

plt.subplot(2,1,2)
plt.plot(t,vels_test[:,1],'b')
plt.plot(t,test_output[:,1],'r')

plt.savefig('pmd_results/closed_training_correlation.svg')
plt.show()


        
                   
        


