import torch
import numpy as np
import csv
import torch.nn as nn
from snn_bptt_batched import snn, SurrGradSpike
from matplotlib import pyplot as plt

import time

dtype = torch.float
device = torch.device('cpu')

reaches = 646
neurons = 46
timebins = 170
vel_scale = 3.82

train_range = 0.6
valid_range = 0.8

labelset = np.zeros((reaches,timebins,2))

EPOCHS = 20
batch_size = 10

output_neurons = 2
layers = [neurons, 65, 40]

learning_rate = 5e-3

dataset = np.zeros((reaches,neurons,timebins))
labelset = np.zeros((reaches,timebins,2))

with open('pmd_data/MT_S2_spikes_processed.csv') as file:
    reader = csv.reader(file)
    index = 0
    for row in reader:
        row_array = np.array(list(map(int,row)))
        reach_table = np.zeros((neurons,timebins))
        
        for i in range(neurons):
            reach_table[i,:] = row_array[i*timebins:(i+1)*timebins]

        dataset[index,:,:] = reach_table
        index += 1

with open('pmd_data/MT_S2_velocities.csv') as file:
    reader = csv.reader(file)
    index = 0
    for row in reader:
        row_array = np.array(list(map(float,row)))
        labelset[index,:,0] = row_array[:timebins]
        labelset[index,:,1] = row_array[timebins:]

        index += 1

timestamp_list = []
with open('pmd_data/MT_S2_timestamps.csv') as file:
    reader = csv.reader(file)
    for row in reader:
        timestamp_list.append(int(row[0]))

labelset = labelset / vel_scale
dataset = dataset.transpose(0,2,1)

train_length = 0
for i in range(int(reaches*train_range)):
    train_length += timestamp_list[i]

test_length = 0
for i in range(int(reaches*valid_range),int(reaches)):
    test_length += timestamp_list[i]

total_label_train = np.zeros((train_length,2))
total_label_test = np.zeros((test_length,2))


curr_index = 0
for trial in range(int(reaches*train_range)):
    timestamp = timestamp_list[trial]
    total_label_train[curr_index:curr_index+timestamp,:] = labelset[trial,:timestamp,:]
    curr_index += timestamp

curr_index = 0
for trial in range(int(reaches*valid_range),int(reaches)):
    timestamp = timestamp_list[trial]
    total_label_test[curr_index:curr_index+timestamp,:] = labelset[trial,:timestamp,:]
    curr_index += timestamp

dataset_train = dataset[:int(reaches*train_range)]
dataset_test = dataset[int(reaches*valid_range):]
labelset_train = labelset[:int(reaches*train_range)]
labelset_test = labelset[int(reaches*valid_range):]

dataset_train = torch.FloatTensor(dataset_train).to(device)
labelset_train = torch.FloatTensor(labelset_train).to(device)
dataset_test = torch.FloatTensor(dataset_test).to(device)
labelset_test = torch.FloatTensor(labelset_test).to(device)


def get_concat(output,isTrain):
    output = output.detach().cpu().numpy()

    shift = 0
    if isTrain:
        total_output = np.zeros((train_length,2))
    else:
        total_output = np.zeros((test_length,2))
        shift = int(valid_range*reaches)

    curr_index = 0
    for trial in range(len(output)):
        timestamp = timestamp_list[trial+shift]
        total_output[curr_index:curr_index+timestamp,:] = output[trial,:timestamp,:]
        curr_index = curr_index + timestamp
        
    return total_output


def get_correlation(total_output,label):

    r = np.corrcoef([total_output[:,0],label[:,0]])
    r_x = r[0,1]

    r = np.corrcoef([total_output[:,1],label[:,1]])
    r_y = r[0,1]

    return r_x,r_y

def savefig_long(output,label,startTime,endTime,epoch):
    output = output * vel_scale
    label = label * vel_scale
    velocities = ['X','Y']
    
    for i in range(2):
        fig = plt.figure()
        plt.plot(label[startTime:endTime,i],'b',label='Actual')
        plt.plot(output[startTime:endTime,i],'r',label='Predicted')
        plt.ylabel(f'{velocities[i]} Velocity (cm/s)')
        plt.xlabel('Time (10 ms)')
        plt.legend(loc="upper right")

        plt.savefig(f'pmd_results/MTS2_e{epoch}_{velocities[i]}_10-09.svg')
            
time_step=1e-2
model = snn(layers,output_neurons,timebins,dropout_threshold=0.0,tau_mem=10.5*time_step,tau_syn=0.215*time_step,time_step=time_step)
#opt = torch.optim.Adam(model.get_weights(),lr=learning_rate,betas=(0.9,0.999))
opt = torch.optim.SGD(model.get_weights(),lr=learning_rate,momentum=0,dampening=0,weight_decay=0,nesterov=False)
#Switch the optimizer to Adam to compare with stochastic gradient descent


loss_fn = torch.nn.MSELoss()

with open(f'pmd_results/MT_S2_bptt.csv', 'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',')
    writer.writerow(['Epoch','Loss','Training X Correlation','Training Y Correlation',
                     'Testing X Correlation','Testing Y Correlation','Average Weight Value'])
    
    for e in range(EPOCHS+1):

        
        total_loss = 0
        for i in range(int(len(dataset_train)/batch_size)):
            data = dataset_train[i*batch_size:(i+1)*batch_size]
            label = labelset_train[i*batch_size:(i+1)*batch_size]

            outputs = model.forward_snn(data,batch_size,apply_dropout=True)
            loss = 0
            for output in outputs:
                local_loss = 0
                for trial in range(len(output)):
                    timestamp = timestamp_list[i*batch_size + trial]
                    
                    local_loss += loss_fn(output[trial,:timestamp,0],label[trial,:timestamp,0])
                    local_loss += loss_fn(output[trial,:timestamp,1],label[trial,:timestamp,1])
                    
                loss += local_loss

            opt.zero_grad()
            loss.backward()
            opt.step()
            total_loss += loss

        if (e % 10 == 0 or e < 10):
            train_outputs = model.forward_snn(dataset_train,len(dataset_train), apply_dropout = False)
            test_outputs = model.forward_snn(dataset_test,len(dataset_test), apply_dropout = False)

            train_output = get_concat(train_outputs[-1],isTrain=True)
            test_output = get_concat(test_outputs[-1],isTrain=False)


            train_xcc,train_ycc = get_correlation(train_output,total_label_train)
            test_xcc_raw,test_ycc_raw = get_correlation(test_output,total_label_test)
            
            total_loss = float("{0:.4f}".format(total_loss))
            train_xcc = float("{0:.4f}".format(train_xcc))
            train_ycc = float("{0:.4f}".format(train_ycc))
            test_xcc = float("{0:.4f}".format(test_xcc_raw))
            test_ycc = float("{0:.4f}".format(test_ycc_raw))

            print(f'Epoch {e} Loss: {total_loss}')
            print(f'Training Avg Coeffs: (X: {train_xcc}, Y: {train_ycc})')
            print(f'Testing Avg Coeffs: (X: {test_xcc}, Y: {test_ycc})')
            print()

            writer.writerow([e,total_loss,train_xcc,train_ycc,test_xcc,test_ycc,weight_mags])

    
savefig_long(test_output,total_label_test,0,3000,e)           
            
        





