import numpy as np
import torch
import csv

dtype = np.float32


class snn():

    def __init__(self,layers,output_neurons,num_timesteps,time_step=1e-3,
                 tau_mem=10e-3,tau_syn=5e-3,tau_ref=5e-3,learning_rate=5e-3):

        self.layers = layers
        self.num_layers = len(layers)
        self.num_timesteps = num_timesteps
        self.output_neurons = output_neurons

        self.sigma_approx = 10
        self.learning_rate = learning_rate

        self.alpha = float(np.exp(-time_step/tau_syn))
        self.beta = float(np.exp(-time_step/tau_mem))

        weight_scale = 5

        self.weights = []
        self.weight_list = []
        for i in range(self.num_layers - 1):
            w = np.random.normal(0,weight_scale,(layers[i],layers[i+1]))
            self.weights.append(w)
            self.weight_list.append(w)

        self.weight_updates = []
        for i in range(len(self.weight_list)):
            self.weight_updates.append(np.zeros(self.weights[i].shape))

        self.ro_weights = []
        for i in range(1,self.num_layers):
            w = np.random.normal(0,weight_scale,(layers[i],self.output_neurons))
            self.ro_weights.append(w)
            self.weight_list.append(w)

    def heaviside(self,x):
        out = np.zeros(x.shape)
        out[x > 0] = 1.0
        return out

    def forward(self,inputs,batch_size,labels,train):
        spk_ins = []
        spk_ins.append(inputs)

        syns = []
        mems = []
        outs = []

        syns_out = []
        mems_out = []
        outputs = []

        P = []
        Q = []

        sigma_U = []
        H = []
        G = []
        for i in range(len(self.weights)):

            syns.append(np.zeros((batch_size,self.layers[i+1]),dtype=dtype))
            mems.append(np.zeros((batch_size,self.layers[i+1]),dtype=dtype))
            outs.append(np.zeros((batch_size,self.layers[i+1]),dtype=dtype))

            sigma_U.append(np.zeros((batch_size,self.layers[i+1]),dtype=dtype))

            syns_out.append(np.zeros((batch_size,self.output_neurons),dtype=dtype))
            mems_out.append(np.zeros((batch_size,self.output_neurons),dtype=dtype))

            P.append(np.zeros((batch_size,self.layers[i]),dtype=dtype))
            Q.append(np.zeros((batch_size,self.layers[i]),dtype=dtype))

            H.append(np.zeros((batch_size,self.layers[i],self.layers[i+1])))
            G.append(np.zeros((batch_size,self.layers[i],self.layers[i+1])))

            
            outputs.append(np.zeros((batch_size,self.num_timesteps,self.output_neurons)))

            spk_ins.append((self.num_timesteps,self.layers[i+1]),dtype=dtype))

            
        for t in range(self.num_timesteps):

            for i in range(len(self.weights)):
                h = np.einsum("ij,jk->ik",spk_ins[i][:,t,:],self.weights[i])
                membrane_threshold = mems[i] - 1.0
                outs[i] = self.heaviside(membrane_threshold)
                reset = outs[i]

                    
                sigma_U[i] = self.sigma_prime(membrane_threshold)

                spk_ins[i+1][:,t,:] = outs[i]

                new_syn = self.alpha*syns[i] + (1-self.alpha)*h
                new_current = self.beta*mems[i] + (1-self.beta)*syns[i]
                new_mem = new_current*(1 - reset)

                new_Q = self.beta*Q[i]+(1-self.beta)*spk_ins[i][:,t,:]
                new_P = self.alpha*P[i] + (1-self.alpha)*Q[i]

                mems[i] = new_mem
                syns[i] = new_syn

                Q[i] = new_Q
                P[i] = new_P


                h_out = np.einsum("ij,jk->ik",outs[i],self.ro_weights[i])
                new_syn_out = self.alpha*syns_out[i] + (1-self.alpha)*h_out
                new_mem_out = self.beta*mems_out[i] + (1-self.beta)*syns_out[i]

                outputs[i][:,t,:] = mems_out[i]
                if (train and t < self.num_timesteps -1):
                    H_new,G_new = self.update_weights(batch_size,i,outputs[i][:,t,:],sigma_U[i],labels[:,t,:],P[i],H[i],G[i])
                    H[i] = Q_p_new
                    H[i] = P_p_new
                        

                syns_out[i] = new_syn_out
                mems_out[i] = new_mem_out

        
            
        return outputs

    def sigma_prime(self,x):
        return self.sigma_approx/(self.sigma_approx*np.abs(x)+1.0)**2

    def update_weights(self,batch_size,layer,output,U,label,P,H,G):
        weights = self.weights[layer]
        ro_weights = self.ro_weights[layer]
        weight_update = np.zeros(weights.shape)

        H_new = np.zeros(H.shape)
        G_new = np.zeros(G.shape)


        for i in range(weights.shape[1]):
            loss_derivative = np.zeros((batch_size))
            for k in range(self.output_neurons):
                loss_derivative[:] += (ro_weights[i,k]*(output[:,k] - label[:,k]))

            for j in range(weights.shape[0]):
                H_new[:,j,i] = self.beta*H[:,j,i] + (1-self.alpha)*U[:,i]*P[:,j]
                G_new[:,j,i] = self.alpha*G[:,j,i] + (1-self.beta)*H[:,j,i]
                        

                for batch in range(batch_size):
                    weight_update[j,i] += loss_derivative[batch]*G_new[batch,j,i]

        self.weights[layer] += -1*self.learning_rate*weight_update
        return H_new, G_new
                

    def get_weights(self):
        return self.weights

    def get_weights_output(self):
        arr = []
        for i in range(len(self.weights)):
            arr.append(self.weights[i])
        arr.append(self.ro_weights[-1])
        return arr

    def get_ro_weights(self):
        return self.ro_weights

    def get_constants(self):
        return self.alpha,self.beta

    def assign_weights(self,weights,ro_weights):
        for i in range(len(weights)):
            self.weights[i] = weights[i]

        for i in range(len(ro_weights)):
            self.ro_weights[i] = ro_weights[i]

            
            
