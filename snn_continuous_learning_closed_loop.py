import numpy as np
import torch
import csv
import gc



dtype = np.float32

class snn():

    def __init__(self,layers,output_neurons,time_step=1e-3,
                 tau_mem=10e-3,tau_syn=5e-3,tau_ref=5e-3,learning_rate=5e-3):

        self.layers = layers
        self.num_layers = len(layers)
        self.output_neurons = output_neurons

        self.sigma_approx = 10
        self.learning_rate = learning_rate

        self.alpha = float(np.exp(-time_step/tau_syn))
        self.beta = float(np.exp(-time_step/tau_mem))

        weight_scale = 5

        self.weights = []
        self.weight_hist = []
        self.weight_list = []
        for i in range(self.num_layers - 1):
            w = np.random.normal(0,weight_scale,(layers[i],layers[i+1]))
            self.weights.append(w)
            self.weight_list.append(w)
            self.weight_hist.append([])

        self.weight_updates = []
        self.weight_update_hist = []
        for i in range(len(self.weight_list)):
            self.weight_updates.append(np.zeros(self.weights[i].shape))
            self.weight_update_hist.append([])
            

        self.ro_weights = []
        for i in range(1,self.num_layers):
            w = np.random.normal(0,weight_scale,(layers[i],self.output_neurons))
            self.ro_weights.append(w)
            self.weight_list.append(w)

        gc.enable()

    def heaviside(self,x):
        out = np.zeros(x.shape)
        out[x > 0] = 1.0
        return out

    #@profile(precision=5)

    #Fix this so that it initializes the arrays at the beginning and then has a separate function to update each time step
    def start_forward_pass(self):

        
        self.spk_ins = []

        self.syns = []
        self.mems = []
        self.outs = []

        self.syns_out = []
        self.mems_out = []
        self.outputs = []

        self.P = []
        self.Q = []

        self.sigma_U = []
        self.H = []
        self.G = []
        for i in range(len(self.weights)):

            self.syns.append(np.zeros((self.layers[i+1]),dtype=dtype))
            self.mems.append(np.zeros((self.layers[i+1]),dtype=dtype))
            self.outs.append(np.zeros((self.layers[i+1]),dtype=dtype))

            self.sigma_U.append(np.zeros((self.layers[i+1]),dtype=dtype))

            self.syns_out.append(np.zeros((self.output_neurons),dtype=dtype))
            self.mems_out.append(np.zeros((self.output_neurons),dtype=dtype))

            self.P.append(np.zeros((self.layers[i]),dtype=dtype))
            self.Q.append(np.zeros((self.layers[i]),dtype=dtype))

            self.H.append(np.zeros((self.layers[i],self.layers[i+1]),dtype=dtype))
            self.G.append(np.zeros((self.layers[i],self.layers[i+1]),dtype=dtype))

            self.spk_ins.append(np.zeros((self.layers[i]),dtype=dtype))

            
    def forward_t(self,inputs,labels,train=False):
        self.spk_ins[0][:] = inputs[:]
        

        for i in range(len(self.weights)):
            h = np.einsum("j,jk->k",self.spk_ins[i],self.weights[i])
            membrane_threshold = self.mems[i] - 1.0
            self.outs[i] = self.heaviside(membrane_threshold)
            reset = self.outs[i]

            self.sigma_U[i] = self.sigma_prime(membrane_threshold)

            if (i+1 < len(self.weights)):
                self.spk_ins[i+1] = self.outs[i]

            new_syn = self.alpha*self.syns[i] + (1-self.alpha)*h
            new_current = self.beta*self.mems[i] + (1-self.beta)*self.syns[i]
            new_mem = new_current*(1 - reset)

            new_Q = self.beta*self.Q[i]+(1-self.beta)*self.spk_ins[i]
            new_P = self.alpha*self.P[i] + (1-self.alpha)*self.Q[i]

            self.mems[i] = new_mem
            self.syns[i] = new_syn

            self.Q[i] = new_Q
            self.P[i] = new_P

            h_out = np.einsum("j,jk->k",self.outs[i],self.ro_weights[i])
            new_syn_out = self.alpha*self.syns_out[i] + (1-self.alpha)*h_out
            new_mem_out = self.beta*self.mems_out[i] + (1-self.beta)*self.syns_out[i]

            if (train):
                H_new,G_new = self.update_weights(i,self.mems_out[i],self.sigma_U[i],
                                                      labels,self.P[i],self.H[i],self.G[i])

                
                self.H[i] = H_new
                self.G[i] = G_new
                        

            self.syns_out[i] = new_syn_out
            self.mems_out[i] = new_mem_out

        return self.mems_out[-1]

    def sigma_prime(self,x):
        return self.sigma_approx/(self.sigma_approx*np.abs(x)+1.0)**2

    def update_weights(self,layer,output,U,label,P,H,G):
        weights = self.weights[layer]
        ro_weights = self.ro_weights[layer]
        weight_update = np.zeros(weights.shape)

        H_new = np.zeros(H.shape)
        G_new = np.zeros(G.shape)


        ld_0 = 0
        for i in range(weights.shape[1]):
            loss_derivative = 0
            for k in range(self.output_neurons):
                loss_derivative += (ro_weights[i,k]*(output[k] - label[k]))

            for j in range(weights.shape[0]):
                Q_new[j,i] = self.beta*H[j,i] + (1-self.alpha)*U[i]*P[j]
                P_new[j,i] = self.alpha*G[j,i] + (1-self.beta)*H[j,i]
                        


                weight_update[j,i] += loss_derivative*G[j,i]

            if i == 0:
                ld_0 = loss_derivative

        self.weights[layer] += -1*self.learning_rate*weight_update
        
        return H_new,G_new
                

    def get_weights(self):
        return self.weights

    def get_weights_output(self):
        arr = []
        for i in range(len(self.weights)):
            arr.append(self.weights[i])
        for i in range(len(self.ro_weights)):
            arr.append(self.ro_weights[i])
        return arr

    def get_ro_weights(self):
        return self.ro_weights

    def get_constants(self):
        return self.alpha,self.beta

    def get_update_hist(self):
        return self.weight_update_hist, self.weight_hist

    def assign_weights(self,weights,ro_weights):
        for i in range(len(weights)):
            self.weights[i] = weights[i]

        for i in range(len(ro_weights)):
            self.ro_weights[i] = ro_weights[i]

            
            
