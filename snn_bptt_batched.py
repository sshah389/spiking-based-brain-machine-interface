import os

import numpy as np

import torch
import torch.nn as nn

dtype = torch.float
device = torch.device('cpu')

#This code is based on Friedemann Zenke's Spytorch tutorial (https://fzenke.net)


class SurrGradSpike(torch.autograd.Function):

    scale = 100.0

    @staticmethod
    def forward(ctx, input):
        """
        In the forward pass we compute a step function of the input Tensor
        and return it. ctx is a context object that we use to stash information which 
        we need to later backpropagate our error signals. To achieve this we use the 
        ctx.save_for_backward method.
        """
        ctx.save_for_backward(input)
        out = torch.zeros_like(input)
        out[input > 0] = 1.0
        return out

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor we need to compute the 
        surrogate gradient of the loss with respect to the input. 
        Here we use the normalized negative part of a fast sigmoid 
        as this was done in Zenke & Ganguli (2018).
        """
        input, = ctx.saved_tensors
        grad_input = grad_output.clone()
        grad = grad_input*SurrGradSpike.scale/(SurrGradSpike.scale*torch.abs(input)+1.0)**2
        return grad


class snn():

    def __init__(self,layers,output_neurons,num_timesteps,time_step=1e-3,tau_mem=10e-3,tau_syn=5e-3,dropout_threshold=0.2):
        self.layers = layers
        self.heaviside = SurrGradSpike.apply
        self.num_layers = len(self.layers)
        self.num_timesteps = num_timesteps
        self.output_neurons = output_neurons

        self.alpha = float(np.exp(-time_step/tau_syn))
        self.beta = float(np.exp(-time_step/tau_mem))

        weight_scale = 7*(1.0-self.beta)
        
        self.dropout_threshold = dropout_threshold


        self.weights = []
        self.weight_list = []
        for i in range(self.num_layers-1):
            w = torch.empty((layers[i],layers[i+1]),  device=device, dtype=dtype, requires_grad=True)
            torch.nn.init.normal_(w, mean=0.0, std=weight_scale/np.sqrt(layers[i]))
            self.weights.append(w)
            self.weight_list.append(w)
                            

        self.ro_weights = []
        for i in range(1,self.num_layers):
            w = torch.empty((layers[i],self.output_neurons),device=device, dtype=dtype,requires_grad=False)
            torch.nn.init.normal_(w, mean=0.0, std=weight_scale/np.sqrt(layers[i]))
            self.ro_weights.append(w)


    def forward_snn(self,inputs,batch_size,apply_dropout = True):
        spk_rec = inputs
        spk_trains = []
        
        dropout = []
        rec_dropout = []
        for i in range(len(self.layers)):
            rand = torch.rand(self.layers[i])
            dropout.append(rand)
            
        for i in range(len(self.weights)):
            weight = torch.clone(self.weights[i])
            rec_weight = torch.clone(self.rec_weights[i])

            if ((i != 0) and (apply_dropout == True)):
                weight[dropout[i] < self.dropout_threshold,:] = 0
                rec_weight[dropout[i+1] < self.dropout_threshold, :] = 0

            spk_rec = spk_rec.detach()
            h_input = torch.einsum("abc,cd->abd", (spk_rec, weight))
            syn = torch.zeros((batch_size,self.layers[i+1]), device=device, dtype=dtype)
            mem = torch.zeros((batch_size,self.layers[i+1]), device=device, dtype=dtype)
            out = torch.zeros((batch_size,self.layers[i+1]), device=device, dtype=dtype)

            mem_rec = torch.zeros((batch_size,self.num_timesteps,self.layers[i+1]),device=device, dtype=dtype)
            spk_rec = torch.zeros((batch_size,self.num_timesteps,self.layers[i+1]),device=device, dtype=dtype)

            
            for t in range(self.num_timesteps):
                h = h_input[:,t]
                membrane_threshold = mem - 1.0
                out = self.heaviside(membrane_threshold)
                reset = out.detach()

                spk_rec[:,t,:] = out
                mem_rec[:,t,:] = mem

                new_syn = self.alpha*syn + 1*h
                new_current = self.beta*mem + 1*syn
                new_mem = new_current*(1- reset)

                mem = new_mem
                syn = new_syn

            spk_trains.append(spk_rec)

        outputs = []
        for i in range(len(spk_trains)):
            ro_weight = torch.clone(self.ro_weights[i])

            if apply_dropout:
                ro_weight[dropout[i+1] < self.dropout_threshold,:] = 0
            
            h = torch.einsum("abc,cd->abd", (spk_trains[i], ro_weight))
            syn = torch.zeros((batch_size,self.output_neurons), device=device, dtype=dtype)
            current = torch.zeros((batch_size,self.output_neurons), device=device, dtype=dtype)
            curr_rec = torch.zeros((batch_size,self.num_timesteps,self.output_neurons),device=device, dtype=dtype)

            for t in range(self.num_timesteps):

                new_syn = self.alpha*syn + 1*h[:,t]
                new_current = self.beta*current + 1*syn

                curr_rec[:,t,:] = current

                syn = new_syn
                current = new_current

            outputs.append(curr_rec)

        return outputs

    def get_weights(self):
        return self.weights

    def get_constants(self):
        return self.alpha, self.beta


