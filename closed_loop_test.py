import numpy as np
import csv, copy
from matplotlib import pyplot as plt

from closed_loop_simulator import CLS
from online_prosthetic_simulator import OPS
from snn_continuous_learning_closed_loop import snn

time_step = 0.01
max_duration = int(3/time_step)
side_radius = 10
max_vel = 20*time_step #cm/s
accel_const = 0.3
min_distance = 8
target_size = 2.5
time_to_target = 0.5/time_step

weights_0 = np.load('closed_loop_data/closed_loop_w0_4.npy')
weights_1 = np.load('closed_loop_data/closed_loop_w1_4.npy')
ro_0 = np.load('closed_loop_data/closed_loop_ro0_4.npy')
ro_1 = np.load('closed_loop_data/closed_loop_ro1_4.npy')

vel_scale = 3.77

cls = CLS(side_radius=side_radius,min_distance=min_distance,max_velocity=max_vel,
          accel_const=accel_const,target_size=target_size)

neurons = 46
learning_rate = 5e-3
ops = OPS(neurons,time_step,upper_lmin=5,lower_lmax=40,upper_lmax=100,
          max_accel=accel_const*max_vel,zero_prob=0.5)

ops.assign_neurons('closed_loop_data/ops_neurons_4.csv')
#This is done so that we can test dropout of the same neurons on a network with and without continuous learning in closed loop

layers = [neurons, 65, 40]
output_neurons = 2

model = snn(layers,output_neurons,learning_rate=learning_rate,
            tau_mem=20*time_step,tau_syn=0.3*time_step,time_step=time_step)

model.assign_weights([weights_0,weights_1],[ro_0,ro_1])


successful_trials = []
target_acq_time = []
pos_total = []
output_total = []
intend_total = []

def run_trials(num_trials,pos_total,output_total,intend_total,successful_trials,target_acq_time):
    for trial in range(num_trials):
        cls.start_trial()
        t = 0
        t_in_range = 0
        
        pos_trial = []
        output_trial = []
        intend_vels = []

        model.start_forward_pass()
        
        while t < max_duration and t_in_range < time_to_target:
            vels,accels = cls.get_velocity()
            pos_trial.append(copy.deepcopy(cls.position))
            intend_vels.append(copy.deepcopy(vels))

            
            spikes = ops.get_spikes(accels)
            vels = vels / time_step
            
            pred_vels = model.forward_t(spikes,vels/vel_scale,train=True)
            output_trial.append(pred_vels)
            
            cls.update_pos(pred_vels*vel_scale*time_step)
            t,t_in_range = cls.get_times()

        if (t_in_range >= time_to_target):
            successful_trials.append(1.0)
        else:
            successful_trials.append(0.0)

        target_acq_time.append(t)

        pos_trial = np.array(pos_trial)
        output_trial = np.array(output_trial)
        intend_vels = np.array(intend_vels)
        intend_vels = intend_vels / time_step
        output_trial = output_trial*vel_scale

        
        pos_total.append(pos_trial)
        output_total.append(output_trial)
        intend_total.append(intend_vels)


    return pos_total,output_total,intend_total,successful_trials,target_acq_time

num_trials = 50
print("Pre Dropout")
pos_total,output_total,intend_total,successful_trials,target_acq_time = run_trials(num_trials,pos_total,output_total,intend_total,successful_trials,target_acq_time)


indx_remove = num_trials

neurons_remaining = list(np.arange(neurons))
num_remove = 25
neurons_remove = []
while len(neurons_remove) < num_remove:
    rm = np.random.choice(neurons_remaining)
    neurons_remaining.pop(neurons_remaining.index(rm))
    neurons_remove.append(rm)

print(neurons_remove)

with open(f'closed_loop_data/removed_neurons.csv','w') as file:
    writer = csv.writer(file)
    writer.writerow(neurons_remove)
'''
with open('closed_loop_data/removed_neurons.csv','r') as file:
    reader = csv.reader(file)
    for row in reader:
        neurons_remove = list(map(int,row))
'''


ops.remove_neurons(neurons_remove)
num_trials = 100
print("Post Dropout")
pos_total,output_total,intend_total,successful_trials,target_acq_time= run_trials(num_trials,pos_total,output_total,intend_total,successful_trials,target_acq_time)

print("Done")


target_acq_time = np.array(target_acq_time)*time_step
successful_trials = np.array(successful_trials)

def get_moving_avg(target_acq_time,window_size):
    acq_time_avg = np.zeros((len(target_acq_time)-window_size))
    for t in range(len(successful_trials)-window_size):
        acq_time_avg[t] = np.mean(target_acq_time[t:t+window_size])
    return acq_time_avg

def plot_time(acq_time_avg,window_size):
    t = np.arange(window_size,len(acq_time_avg)+window_size)
    plt.plot(t,acq_time_avg)
    plt.ylim([0,3])
    plt.xlabel("Trial")
    plt.ylabel("Average Time to Target (s)")
    plt.axhline(y=0.74,color='r',linestyle='dashed',label="Minimum Possible Time")

    #plt.axvline(x=indx_remove,color='g',label='Neurons Removed')
    plt.show()
    

acq_time_avg4 = get_moving_avg(target_acq_time,4)
acq_time_avg10 = get_moving_avg(target_acq_time,10)

print(np.sum(successful_trials)/len(successful_trials))

with open('closed_loop_results/closed_loop_change_neurons_w4.csv','w') as file:
    writer = csv.writer(file)
    writer.writerow(list(acq_time_avg4))

with open('closed_loop_results/closed_loop_change_neurons_w10.csv','w') as file:
    writer = csv.writer(file)
    writer.writerow(list(acq_time_avg10))
        

plot_time(acq_time_avg4,4)
plot_time(acq_time_avg10,10)


plt.plot(t,acq_time_avg)
plt.ylim([0,3])
plt.xlabel("Trial")
plt.ylabel("Average Time to Target (s)")
plt.axhline(y=0.74,color='r',linestyle='dashed',label="Minimum Possible Time")
plt.axvline(x=indx_remove,color='g',label='Neurons Removed')

plt.savefig('pmd_results/closed_loop_decolle_both_figs.svg')
plt.show()



    
    

        
        

