import numpy as np
import time, csv

def normalize(vector):
    mag = np.linalg.norm(vector)
    if (mag == 0):
        return vector
    else:
        return vector / mag

class CLS():

    def __init__(self,side_radius=10,min_distance=0,max_velocity=10,accel_const=0.2,target_size=1):
        self.side_radius = side_radius
        self.max_vel = max_velocity
        self.accel_const = accel_const
        self.target = [0.0,0.0]
        self.target_size = target_size
        self.min_distance = min_distance
        self.distance_const = self.max_vel / np.sqrt(2*self.target_size) #Velocity peaks when you're half the side length away from an object

    def start_trial(self):
        target_mag = self.min_distance
        target_angle = np.random.uniform(-np.pi,np.pi)

        self.target = target_mag*np.asarray([np.cos(target_angle),np.sin(target_angle)])
        self.position = np.array([0.0,0.0])
        self.velocity = np.array([0.0,0.0])
        self.t = 0
        self.time_in_range = 0

    def get_velocity(self):
        vector = self.target - self.position
        angle = normalize(vector)
        vel_mag = min(self.distance_const*np.sqrt(np.linalg.norm(vector)),self.max_vel)
        new_velocity = vel_mag*angle

        delta_velocity = (new_velocity - self.velocity)*self.accel_const
        #The acceleration constant prevents instantaneous jumps in velocity

        new_velocity =  self.velocity + delta_velocity
        return new_velocity,delta_velocity

    def update_pos(self,new_vel):
        self.position += new_vel
        self.velocity = new_vel
        self.t += 1
        target_dist = np.linalg.norm(self.position - self.target)
        if (target_dist < self.target_size):
            self.time_in_range += 1
        else:
            self.time_in_range = 0

    def get_times(self):
        return self.t, self.time_in_range
        
        
