This repository contains code for a continuous learning spiking neural network for use in a brain machine interface. The actual SNN code is found in "snn_continuous_learning.py" and "snn_continuous_learning_batched.py" The SNN is tested in "continuous_learning_evaluation.py" and the equivalent file for multiple batches. Note that in order to run this code, you must download the pmd-1 dataset from the CRCNS data sharing initiative (https://crcns.org/data-sets/motor-cortex/pmd-1). The input data format is a 646 by 7820 csv array, where each row represents an individual reaching trial. Each reach was zero-padded to reach 170 timebins, each of length 10 ms. The 7820 dimension comes from concatenating the 170 timestamps for each of the 46 input neurons.

The repository also contains code for a closed loop simulation. This simulation uses a variant of the SNN code that has been modified to allow reading of the output value after each time step, to allow interacting with the closed loop system. Simulated neural activity is generated in "online_prosthetic_simulator.py", while simulated kinematic data is generated in "closed_loop_simulator.py". The SNN can be trained offline on simulated reaches in "closed_loop_trainer.py" and tested in a closed loop environment in "closed_loop_test.py"

For comparison purposes, a backpropagation-through-time SNN is provided that uses Pytorch for learning. This SNN is based on an implementation developed by Friedemann Zenke (https://github.com/fzenke/spytorch/). The SNN is stored in "snn_bptt.py" and "snn_bptt_batched.py", and called in "bptt_evaluation.py". The input format of this code is the same as the one for the continuous learning offline evaluation.

If you have any questions about this data set or the corresponding paper, feel free to contact the author at etaeckens@gmail.com.

If you use this gitlab repository in your work please cite the following paper:

Elijah A Taeckens and Sahil Shah "A spiking neural network with continuous local learning for robust online brain machine interface " Journal of Neural Engineering December 20 2023 doi: 10.1088/1741-2552/ad1787.
