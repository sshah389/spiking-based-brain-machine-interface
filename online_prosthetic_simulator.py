import numpy as np
#from matplotlib import pyplot as plt
import time, csv

def normalize(vector):
    mag = np.linalg.norm(vector)
    if (mag == 0):
        return vector
    else:
        return vector / mag

class OPS():

    def __init__(self,num_neurons,time_step,upper_lmax,lower_lmax,upper_lmin,max_accel,zero_prob = 0):
        #upper_lmin is the upper bound for the minimum firing rate of a neuron
        #lower_lmax is the lower bound for the maximum firing rate
        #upper_lmax is the upper bound for the maximum firing rate
        
        self.num_neurons = num_neurons
        self.time_step = time_step
        self.max_accel = max_accel
        self.neurons = []
        for i in range(num_neurons):
            self.neurons.append(Synthetic_Neuron(time_step,upper_lmax,lower_lmax,upper_lmin,max_accel,zero_prob=zero_prob))

    def get_spikes(self,v_t):
        spikes= np.zeros((self.num_neurons,))
        for i in range(self.num_neurons):
            spikes[i] = self.neurons[i].get_spike(v_t)
        return spikes

    def remove_neurons(self,indices):
        for i in indices:
            self.neurons[i].removed = True

    def save_neurons(self,filename):
        with open(filename,'w') as file:
            writer = csv.writer(file)
            for i in range(len(self.neurons)):
                neuron = self.neurons[i]
                writer.writerow([neuron.c[0],neuron.c[1],neuron.lambda_min,neuron.lambda_max])

    def assign_neurons(self,filename):
        self.neurons = []
        with open(filename,'r') as file:
            reader = csv.reader(file)
            for row in reader:
                c = np.array([float(row[0]),float(row[1])])
                lambda_min = float(row[2])
                lambda_max = float(row[3])

                neuron = Synthetic_Neuron_Accel(self.time_step,100,0,10,self.max_accel)
                neuron.assign(c,lambda_min,lambda_max)
                self.neurons.append(neuron)

        self.num_neurons = len(self.neurons)
                
        

class Synthetic_Neuron():
    
    def __init__(self,time_step,upper_lmax,lower_lmax,upper_lmin,max_accel,zero_prob=0):
        self.max_accel = max_accel
        self.time_step = time_step
        
        zero_choice = np.random.choice([0,1],p=[zero_prob,1-zero_prob])
        self.lambda_min = np.random.uniform(0,upper_lmin)*zero_choice

        self.lambda_max = np.random.uniform(max(self.lambda_min,lower_lmax),upper_lmax)

        self.theta_prefer = np.random.uniform(-np.pi,np.pi)
        self.c = np.asarray([np.cos(self.theta_prefer),np.sin(self.theta_prefer)])


        self.removed = False

    def assign(self,c,lambda_min,lambda_max):
        self.c = c
        self.lambda_min = lambda_min
        self.lambda_max = lambda_max

    def get_spike(self,v_t):
        inner_prod = min(1,max(0,1.5*np.inner(self.c,v_t))/self.max_accel)
        lambda_t = (self.lambda_max-self.lambda_min)*inner_prod + self.lambda_min

            
        p = lambda_t * self.time_step
        if self.removed:
            p = 0

        return np.random.choice([0,1],p=[1-p,p])


        
