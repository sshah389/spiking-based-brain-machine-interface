import numpy as np
import csv

from snn_continuous_learning import snn
from matplotlib import pyplot as plt

import time

reaches = 646
neurons = 46
timebins = 170
vel_scale = 3.82
#vel_scale determined with Bayesian optimization
#Scaling factor to get velocity values in a range that LIF membrane potential can reach

train_range = 0.6
valid_range = 0.6

labelset = np.zeros((reaches,timebins,2))

EPOCHS = 1

layers = [neurons, 65, 40]
output_neurons = 2

learning_rate = 5e-3

dataset = np.zeros((reaches,neurons,timebins))
labelset = np.zeros((reaches,timebins,2))

with open('pmd_data/MT_S2_spikes_processed.csv') as file:
    reader = csv.reader(file)
    index = 0
    for row in reader:
        row_array = np.array(list(map(int,row)))
        reach_table = np.zeros((neurons,timebins))
        
        for i in range(neurons):
            reach_table[i,:] = row_array[i*timebins:(i+1)*timebins]

        dataset[index,:,:] = reach_table
        index += 1

with open('pmd_data/MT_S2_velocities.csv') as file:
    reader = csv.reader(file)
    index = 0
    for row in reader:
        row_array = np.array(list(map(float,row)))
        labelset[index,:,0] = row_array[:timebins]
        labelset[index,:,1] = row_array[timebins:]

        index += 1

timestamp_list = []
with open('pmd_data/MT_S2_timestamps.csv') as file:
    reader = csv.reader(file)
    for row in reader:
        timestamp_list.append(int(row[0]))

labelset = labelset / vel_scale
dataset = dataset.transpose(0,2,1)

train_length = 0
for i in range(int(reaches*train_range)):
    train_length += timestamp_list[i]

test_length = 0
for i in range(int(reaches*valid_range),int(reaches)):
    test_length += timestamp_list[i]

total_label_train = np.zeros((train_length,2))
total_label_test = np.zeros((test_length,2))
total_data_train = np.zeros((train_length,neurons))
total_data_test = np.zeros((test_length,neurons))


#The reaches vary in duration, so we use this code to get them into one continuous array
curr_index = 0
for trial in range(int(reaches*train_range)):
    timestamp = timestamp_list[trial]
    total_label_train[curr_index:curr_index+timestamp,:] = labelset[trial,:timestamp,:]
    total_data_train[curr_index:curr_index+timestamp,:] = dataset[trial,:timestamp,:]
    curr_index += timestamp

curr_index = 0
for trial in range(int(reaches*valid_range),int(reaches)):
    timestamp = timestamp_list[trial]
    total_label_test[curr_index:curr_index+timestamp,:] = labelset[trial,:timestamp,:]
    total_data_test[curr_index:curr_index+timestamp,:] = dataset[trial,:timestamp,:]
    curr_index += timestamp


def get_correlation(total_output,label):

    r = np.corrcoef([total_output[:,0],label[:,0]])
    r_x = r[0,1]

    r = np.corrcoef([total_output[:,1],label[:,1]])
    r_y = r[0,1]

    return r_x,r_y
            
time_step=1e-2
model = snn(layers,output_neurons,tau_mem=10.5*time_step,learning_rate=5e-3,
            tau_syn=0.215*time_step,time_step=time_step)


window = 60
window_size = int(window/time_step)

writefile = f'pmd_results/MT_S2_sliding_window_continuous.csv'
with open(writefile, 'w') as csvfile:
    writer = csv.writer(csvfile,delimiter=',')
    
    if (True):
        train_output = model.forward(total_data_train,total_label_train,total_data_train.shape[0], train=True)[-1]
        test_output = model.forward(total_data_test,total_label_test,total_data_test.shape[0], train=False)[-1]

        correlations = np.zeros((len(train_output)+len(test_output),2))
        for t in range(0,len(train_output)-window_size):
            output_slice = train_output[t:t+window_size,:]
            label_slice = total_label_train[t:t+window_size,:]
            corr_x,corr_y = get_correlation(output_slice,label_slice)
            correlations[t+window_size,0] = corr_x
            correlations[t+window_size,1] = corr_y

        for t in range(len(train_output)-window_size,len(train_output)):
            test_t = t - len(train_output) + window_size
            output_slice = np.concatenate((train_output[t:,:],test_output[:test_t,:]),axis=0)
            label_slice = np.concatenate((total_label_train[t:,:],total_label_test[:test_t,:]),axis=0)

            corr_x,corr_y = get_correlation(output_slice,label_slice)
            correlations[t+window_size,0] = corr_x
            correlations[t+window_size,1] = corr_y

        for t in range(len(test_output)-window_size):
            output_slice = test_output[t:t+window_size,:]
            label_slice = total_label_test[t:t+window_size,:]

            corr_x,corr_y = get_correlation(output_slice,label_slice)
            correlations[t+window_size+len(train_output),0] = corr_x
            correlations[t+window_size+len(train_output),1] = corr_y

        t = np.arange(len(correlations))*time_step
        plt.plot(t,correlations[:,0],'b')
        plt.plot(t,correlations[:,1],'r')
        plt.axvline(x=len(train_output)*time_step,color='g',label='End of Training')
        plt.xlabel('Time (s)')
        plt.ylabel('Pearson Correlation')

        writer.writerow(['X','Y'])
        for t in range(len(correlations)):
            writer.writerow(correlations[t,:])
            
        plt.show()



        train_xcc,train_ycc = get_correlation(train_output,total_label_train)
        test_xcc_raw,test_ycc_raw = get_correlation(test_output,total_label_test)
            
        train_xcc = float("{0:.4f}".format(train_xcc))
        train_ycc = float("{0:.4f}".format(train_ycc))
        test_xcc = float("{0:.4f}".format(test_xcc_raw))
        test_ycc = float("{0:.4f}".format(test_ycc_raw))

        print(f'Training Avg Coeffs: (X: {train_xcc}, Y: {train_ycc})')
        print(f'Testing Avg Coeffs: (X: {test_xcc}, Y: {test_ycc})')
        print()
            
        





